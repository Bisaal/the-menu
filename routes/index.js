var express = require('express');
var router = express.Router();
var jsonData = require('../dump/sample');


var jsonData;

/*
  ROUTS
    -these are the routs for the home page
*/
// HOME PAGE 🏠
console.log(jsonData.restaurant.id);

router.get('/', function (req, res, next) {
  res.render('main', { data: jsonData })
});


module.exports = router;
