var jsonData = {
	"restaurant": {
		"id": 2164532165,
		"name": "Green chillies",
		"Address": "B temple road, chandigarh"
	},
	"category": [
		{
			"id": 21645,
			"color": "#FFFFFF",
			"name": "Dessert",
			"items": [
				{
					"id": 123695947,
					"name": "Australian Chocolate Slice",
					"type": "veg",
					"image": "Australian_Chocolate_Slice.jpg",
					"status": "",
					"price": 10
				},
				{
					"id": 123698447,
					"name": "Chilean Sopaipillas with Pebre",
					"type": "veg",
					"image": "pebre-y-sopaipillas.jpg",
					"status": 2,
					"price": 8
				},
				{
					"id": 123698544,
					"name": "Persian Love Cake Cupcakes",
					"type": "veg",
					"image": "Persian_Love_Cakes.jpg",
					"status": "",
					"price": 10
				}
			]
		},
		{
			"id": 21689,
			"color": "#B6B6B6",
			"name": "Drinks",
			"items": [
				{
					"id": 123695988,
					"name": "Peru – Pisco Sour",
					"type": "veg",
					"image": "Peru_Pisco_Sour.jpg",
					"status": "",
					"price": 5
				},
				{
					"id": 123698499,
					"name": "Brazil – Caipirinha",
					"type": "veg",
					"image": "Brazil_Caipirinha.jpg",
					"status": "",
					"price": 7
				},
				{
					"id": 123698500,
					"name": "Italy – Campari",
					"type": "non-veg",
					"image": "Italy_Campari.jpg",
					"status": "",
					"price": 6
				}
			]
		},
		{
			"id": 21159,
			"color": "#707070",
			"name": "Breads",
			"items": [
				{
					"id": 123695981,
					"name": "Focaccia",
					"type": "veg",
					"image": "Focaccia.jpg",
					"status": "",
					"price": 5
				},
				{
					"id": 123698491,
					"name": "Pita",
					"type": "veg",
					"image": "Pita.jpg",
					"status": "",
					"price": 3
				},
				{
					"id": 123698501,
					"name": "Lavash",
					"type": "veg",
					"image": "Lavash.jpg",
					"status": "",
					"price": 5
				}
			]
		}
	]
}

module.exports = jsonData;